dbfilename = 'people-file.txt'
ENDDB = 'enddb.'
ENDREC = 'endrec.'
RECSEP = ' => '


def storeDBase(db, dbfilename=dbfilename):
	"Save database into file"
	dbfile = open(dbfilename, 'w')
	for key in db:
		print (key, file=dbfile)
		# print >> dbfile, key 
		for (name, value) in db[key].items():
			# print >> dbfile, name + RECSEP + repr(value)
			print(name + RECSEP + repr(value), file=dbfile) 
		# print >> dbfile, ENDREC ## v2.7
		print(ENDREC, file=dbfile) ## v3.~
	# print >> dbfile, ENDDB
	print(ENDDB, file=dbfile)
	dbfile.close()

def loadDBase(dbfilename = dbfilename):
	"Recover data, rebuilding database"
	dbfile = open(dbfilename)
	import sys
	sys.stdin = dbfile
	db = {}
	key = input() ## gets one row from stdin
	while key != ENDDB:
		rec = {}
		field = input()
		while field != ENDREC:
			name, value = field.split(RECSEP)
			rec[name] = eval(value)
			field = input()
		db[key] = rec
		key = input()
	return db	

if __name__ == '__main__':
	from initdata import db
	storeDBase(db)		