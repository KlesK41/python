__author__ = 'Andriy'
import shelve
from person_start import Person
fieldnames = ('name', 'age', 'pay', 'job')

db = shelve.open('class-shelve')

while True:
    key = input('\nKey?=>')
    if not key: break
    if key in db:
        record = db[key]
    else:
        record = Person(name='?', age='?')
    for field in fieldnames:
        curval = getattr(record, field)
        newtext = input('\t[%s]=%s\n\tnew?=>' % (field, curval))
        if newtext:
            setattr(record, field, eval(newtext))
    db[key] = record
db.close()


